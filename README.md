# Prise de rendez-vous

Plug-in Drupal créé pour faciliter la gestion des rendez-vous des collectivités territoriales, cet outil permet aux citoyens la prise en ligne de rendez-vous et aux agents en charge de les gérer en backoffice. 

## Financement 

Ce projet a été rendu possible grâce au soutien de :

![Financeurs PRDV](./prdv-financeurs.png)

* La Préfecture de la Région Bourgogne-Franche-Comté
* La Région Bourgogne-Franche-Comté
* L'Agence Régionale du Numérique et de l'intelligence artificielle (ARNia)
* La ville d'Auxonne

## BackOffice 

Les  principales caractéristiques du BackOffice sont : 

* Gestion de rendez-vous
* Agenda type Calendar
* Configuration et gestion des types de rendez-vous, des agents, des guichets et des disponibilités / indisponibilités
* Configuration et gestion de l’API SMS permettant de faire rappels des rendez-vous. 

**N.B. :** L’envoi de SMS nécessite d’un abonnement chez un fournisseur de SMS.


## FrontOffice 

Les  principales caractéristiques du FrontOffice sont :

* Interfaces adaptées aux terminaux mobiles
* Intégration automatique au site Internet dès activation du plug-in 
* Emplacement pour l’affichage d’informations réglementaires (CNI, RGPD, …)
* Affichage et sélection des créneaux disponibles
* Envoi d’email de confirmation et de rappel SMS 


## Déploiement de l’outil de prise de rendez-vous

L’outil de prise de rendez-vous est un module Drupal. Une fois son installation réalisée par l’administrateur Drupal, la configuration nécessaire à son bon fonctionnement est mise en place.

Dans la partie Back Office, un nouvel onglet apparaît dans le tableau de bord, où il est possible d’effectuer toute la gestion du module.

Dans la partie Front Office, un lien est généré, pour pouvoir accéder au module. Le lien existe, cependant, certaines actions sont nécessaires pour que ce lien apparaisse dans votre site web. L’objectif de ce document est de décrire ces actions. Il existe 2 possibilités : soit vous êtes rattachés à CMonSite, soit vous ne l’êtes pas.

Prérequis : Le module est installé dans Drupal. L’utilisateur est un webmaster, ayant accès au back office Drupal pour la création de contenus.

Si vous n’êtes pas rattachés à CMonSite, le frontOffice du module sera directement accessible à l’URL : [votre site]/rendez-vous
Vous pouvez suivre les étapes suivantes pour créer un lien vers cette url.

Si vous êtes rattachés à CMonSite :

1.	Entrer dans le lien [url de votre site]/rendez-vous. Vous devez voir la page du choix de type de rendez-vous. Si ce n’est pas le cas, cela signifie que le module est mal installé. Approchez-vous de l’administrateur de votre site pour l’installer.
2.	Entrer dans le Back Office, puis dans le menu « Menus »

![image.png](./image.png)

3.	Entrer dans le menu « Navigation principale »

![image-1.png](./image-1.png)

4.	Entrer dans « Ajouter un lien »

![image-2.png](./image-2.png)

5.	Remplissez les informations. Dans le champ « Lien », vous devez entrer « /rendez-vous ». Pour le reste, vous pouvez configurer le nouveau menu comme vous le souhaitez.

![image-3.png](./image-3.png)

Une fois le nouveau lien enregistré et déployé sur votre site, vous pourrez accéder depuis le site à la page de prise de rendez-vous.

## Calendrier

Le calendrier de l'outil utilise la version gratuite mise à disposition des organisations à but non lucratif de la librarie DHTMLX Calendar https://dhtmlx.com/docs/products/dhtmlxCalendar/

## Notice technique 

Vous pouvez consulter la documentation technique ici : [Notice technique](./PRDV-Doc-Technique-1.0.pdf) 

## Code 

Vous trouverez le code du plug-in dans [appointment-2022-07-08.zip](appointment-2022-07-08.zip)

## Prestataire d'intégration 

Les développements de ce projet ont été réalisés par ATOL CD : https://www.atolcd.com/

## Licence

[CECILL-2.1](http://www.cecill.info/licences/Licence_CeCILL_V2.1-fr.html)

## Statut du projet
Projet en cours de déploiement chez l'ARNia. 
